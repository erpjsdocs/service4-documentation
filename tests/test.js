const fs = require('q-io/fs');

import { assert } from 'chai';
const DIR = './../docs';

async function getFiles(regex) {
    const files = await fs.listTree(DIR, (path, stat) => !stat.isDirectory());


    return files.filter(file => regex.test(file));
}
suite('Location files');

test('Images', async () => {
    const files = await getFiles(/jpg|png/);

    for (let file of files) {
        const relative = await fs.relative(DIR, file);
        const parent = fs.split(relative)[0];

        assert.equal(parent, 'img', 'images must be in img directory');
    }
});
test('Markdown', async () => {
    const files = await getFiles(/\.md$/);

    for (let file of files) {
        const relative = await fs.relative(DIR, file);
        const parent = fs.split(relative)[0];

        assert.notEqual(parent, 'img', 'markdown must not be in the img directory');
    }
});

test('Unknown files', async () => {
    const files = await getFiles(/./);
    const unknown = files.filter(file => {
        const isRM = /\.md$/.test(file);
        const isPdf = /\.pdf$/.test(file);
        const isJpg  = /jpg|png/.test(file);


        return !(isRM || isJpg || isPdf);
    });

    assert.lengthOf(unknown, 0, 'there should be only md, pdf and img files');
});

suite('Markdown files');

test('Markdown file name', async () => {
    const files = await getFiles(/\.md$/);

    for (let file of files) {
        const name = fs.base(file, '.md');

        assert.ok(!/[^\w\s]/.test(name), 'names of .md files should consist only latin letters');
    }
});

test('Markdown file title', async () => {
    const files = await getFiles(/\.md$/);

    for (let file of files) {
        const starts = await fs.read(file, {
            begin:0,
            end  :10
        });

        assert.ok(/^#\s/.test(starts), '.md files should starts with title');
    }
});

