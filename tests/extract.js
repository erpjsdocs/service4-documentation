const fs = require('q-io/fs');
const { trimJsCode, getJsCode, getFiles } = require('./utils.js');
const DIR = './../docs';
const CODE = './code';

async function makeFiles(dir) {
    const files = await getFiles(DIR, /\.md$/);
    for (let file of files) {
        const content = await getJsCode(file);
        if (content) {
            const relative = await fs.relative(DIR, file);
            const folder  = await fs.join(dir, relative);
            await fs.makeTree(folder);
            for (let code in content) {
                let bunch = trimJsCode(content[code]);
                const filePath = fs.join(folder, `bunch_${code}.js`);
                await fs.write(filePath, bunch);
            }
            console.log(`${folder} done with ${content.length} bunches`);
        }
    }
}

makeFiles(CODE);
