const fs = require('q-io/fs');

exports.trimJsCode = (code) => {
    return code
        .replace(/```javascript\n/, '')
        .replace(/```$/, '');
};

exports.wrapJsCode = (code) => {
    return '```javascript\n' + code + '```';
};

exports.getJsCode =  async (file) => {
    const content = await fs.read(file);
    const regex = /```javascript\n([^`]*)\n```/g;

    return content.match(regex);
};

exports.getFiles = async (dir, filter) => {
    const files = await fs.listTree(dir);

    return files.filter(file => filter.test(file));
};
