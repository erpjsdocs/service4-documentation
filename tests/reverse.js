const fs = require('q-io/fs');
const { wrapJsCode, getFiles } = require('./utils.js');
const DIR = './../docs';
const CODE = './code';

async function reverse(dir) {
    const files = await getFiles(DIR, /\.md$/);
    for (let file of files) {
        const relative = await fs.relative(DIR, file);
        const folder = await fs.join(dir, relative);
        const jsFiles = await getFiles(folder, /\.js$/);
        jsFiles.sort((a, b) => {
            const nameA = fs.base(a, '.js');
            const nameB = fs.base(b, '.js');
            return +nameA.substr(6) - nameB.substr(6);
        });
        const content = await fs.read(file);

        let match = 0;
        const arr = {};

        const func  = () => {
            return wrapJsCode(arr[match++]);
        };

        for (let jsFile in jsFiles) {
            const jscont = await fs.read(jsFiles[jsFile]);
            arr[jsFile] = jscont;
        }

        const replaced = content.replace(/```javascript\n([^`]*)\n```/g, func);

        await fs.write(file, replaced);

        console.log(`${folder} done`);
    }
}

reverse(CODE);
