# Перемещения

Регистр для учета складских перемещений товаров.

## Переменные для печати

- Со_Склада_Код
- Со_Склада_Счет_Склада
- Со_Склада_Наименование
- Со_Склада_Объекты
- На_Склад_Код
- На_Склад_Счет_Склада
- На_Склад_Наименование
- На_Склад_Объекты
- Перемещение_Номер
- Перемещение_Дата_Операции
- Перемещение_Коментарий
- Перемещение_Статус_Приёмки
- Перемещение_Объекты
- Перемещение_Всего_Количество
- Перемещение_Матрица_Номер
- Перемещение_Матрица_Товар
- Перемещение_Матрица_Количество
- Перемещение_Матрица_Описание
- Перемещение_Матрица_Объекты
- Перемещение_Матрица_Себестоимость
- Перемещение_Матрица_Серийный_номер
