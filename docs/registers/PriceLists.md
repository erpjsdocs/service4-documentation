# Прейскуранты

Регистр содержит базовую информацию о таблице цен.

Обязательные поля для создания записи цены:

- Код

## Описание полей:

- <B>Код</B>: код таблицы .
- <B>Период</B>: Два поля "с даты", "по дату" (период действия).
- <B>Время</B>: Два поля "с определенного времени", "по определенное время" (период действия).
- <B>Цена</B>: цена на товар по конкретной таблице цен.
- <B>Заменяет</B>:
- <B>Продавец</B>: привязка к конкретному продавцу.
- <B>Стоимость</B>: стоимость товара.
- <B>Счет сдачи</B>: код счета сдачи.

### НДС

- <B>Не включено;</B>
- <B>Включено с налогом</B>
- <B>включено</B>

### Тип

- <B>Со скидкой;</B>
- <B>Со скидкой от кол-ва;</B>
- <B>Обратная покупка</B>

## Описание колонок таблицы:

- <B>Код товара</B>: код товара или код группы товара.
- <B>Формула</B>: формула расчета цены.
- <B>Тип строки</B>: определение типа строки: для группы товара или для товара.
- <B>Код НДС;</B>
- <B>Налоговый шаблон;</B>
