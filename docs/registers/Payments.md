# Платежи клиентов

Регистр служит для фиксации денежного оборота с клиентом.

Описание полей:

- <B>№</B>: Порядковый номер документа. Внутренний. Генерируется автоматически.
- <B>Дата операции</B>: Дата финансовой операции.
- <B>Способ оплаты</B>: Связь платежа с настройкой "Способы оплаты". Служит для связывания документа с финансовыми счетами и объектами кассы.
