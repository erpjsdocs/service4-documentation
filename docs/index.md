# ERPJS-4: Сервис
_____________

## ERPJS-4: Сервис - решение для управления сервисным центром

Програмное обеспечение "ERPJS-4: Сервис" разработанно для информационной поддержки работы сервисно-ремонтного центра.

### Система позволяет: 
- Создавать заявки на обслуживание и ремонт от физических лиц и организаций.
- Фиксировать работу сервисных инженеров и расход материалов и запчастей.
- Вести учет клиетов и поставщиков.
- Сладской учет нескольких складов.
    - Учет товарной себестоимости по методу FIFO.
    - Учет серийных номеров товаров.
- Розничные продажи товаров и услуг.
- Вести финансовый учет в разрезе плана счетов.
- Контролировать историю контакта и вести систему лояльности
- Контроль цен на товары и услуги; индивидуальные прейскуранты для разных групп клиентов.

# ERPJS-4: Интерфейс
## Основные элементы интерфейса
### Главное окно

![ERPJS Main Window](img/MainWindow.png)

- 1 - Логотип. Является ссылкой на документацию по системе ERPJS
- 2 - Кнопка выбора активного модуля. В зависимости от прав доступа пользователя в выпадающем списке доступны необходимые модули для навигации. Текущий активный модуль отображается справа от кнопки.
- 3 - Кнопка выбора регистра. В зависимости от выбранного модуля и прав доступа пользователя в списке доступны регистры для работы с ними.
- 4 - Кнопка выбора настроек. В зависимости от выбранного модуля и прав доступа пользователя в списке доступны настройки для работы с ними.
- 5 - Кнопка выбора отчетов. Список зависит от выбранного модуля и прав доступа пользователя.
- 6 - Кнопка выбора процедур. Список зависит от выбранного модуля и прав доступа пользователя.
- 7 - Кнопка вызова окна Управления процессами.
- 8 - Кнопка управления входом в систему. Отображается текущий логин пользователя. В выпадающем меню доступна смена пароля пользователя а также выход из системы.
- 9 - Иконка текущего состояния подключения к серверу.
- 10 - Кнопка вызова меню ссылок. С помощью добавления ссылок на документы возможен быстрый переход к выбранным документам или окнам.
- 11 - Кнопка вызова окна списка задач пользователя.
- 12 - Кнопка обратной связи с разработчиками. С помощью нее можно отправить сообщение об ошибке программы. 

### Регистр - список

![ERPJS Main Window](img/Register-list.png)

- 1 - Название регистра
- 2 - Кнопка добавления окна в меню ссылок для быстрого доступа. Ссылка на это окно появится в меню ссылок в главном окне. (См. п. 10 главного окна)
- 3 - Поле для полнотекстового поиска по записям регистра. Поиск не учитывает содержимое полей матрицы записи.
- 4 - Кнопка дубликат служит для создания копии выделенной записи. При нажатии кнопки открывается окна просмотра записи с заполненными данными из выбранной записи. Чтобы данные записались в базу данных нужно сохранить открывшуюся запись. При дублировании данных некоторые поля, дублирование которых запрещено копировать логически очищаются либо рассчитываются заново.
- 5 - Кнопка для создании новой записи в регистре.
- 6 - Шапка списка записей в регистре. Можно масштабировать перетаскиванием мыши. При нажатии на наименование столбца записи в таблице отсортируются в по этому столбцу.
- 7 - Существующие записи в регистре. Могут быть ограничены правами доступа или другими фильтрами. Чтобы открыть запись нужно выбрать ее и сделать двойное нажатие мыши или нажать клавишу Enter.

### Регистр - просмотр записи

![ERPJS Main Window](img/Register-detail.png)

- 1 - Название регистра открытой записи.
- 2 - Состояние редактирования записи. ПРОСМОТР - запись открыта на просмотр. РЕДАКТИРОВАНИЕ - данные в открытой записи изменены и требуют сохранения в базу данных. Запись в режиме редактирования блокируется для редактирования другими пользователями. ДУБЛИРОВАНО - состояние окна при создании новой записи методом копирования существующей. НОВЫЙ - статус записи при создании новой записи до первого сохранения.
- 3 - Кнопка вызова справки по существующему документу.
- 4 - Кнопка добавления окна в меню ссылок для быстрого доступа. Ссылка на это окно появится в меню ссылок в главном окне. (См. п. 10 главного окна)
- 5 - Кнопка для создания новой записи в текущем регистре. Предварительно нужно перевести состояние окна в статус ПРОСМОТР сохранением или отменой действий.
- 6 - Кнопка сохранить - для сохранения изменений.
- 7 - Кнопка печати документа. Активна если для документа предусмотрена печатная процедура.
- 8  - Вызов меню процедур доступных для записей текущего регистра. Индивидуальный список для каждого окна.
- 9 - Список связанных документов. Некоторые записи могут быть логически связаны с другими записями. Эту связь можно просмотреть с помощью этой кнопки. Также можно создать связи вручную методом перетаскивания иконки кнопки на эту же иконку другой открытой записи.
- 10 - Список загруженных файлов. К каждой записи в системе можно подгрузить внешние документы (pdf, картинки, другие файлы).
- 11 - Кнопки навигации по записям регистра. При нажатии кнопки происходит переход к следующей или предыдущей записи в регистре.
- 12 - Кнопка дубликат служит для создания копии открытой записи. При нажатии кнопки данные текущей записи копируются в новую. Чтобы данные записались в базу данных нужно сохранить новую запись. При дублировании данных некоторые поля, дублирование которых запрещено копировать логически очищаются либо рассчитываются заново.
- 13 - Кнопка отмены внесенных изменений.
- 14 - Кнопка удаления записи. С предварительным подтверждением удаления.

В некоторых окнах списка записей могут быть кнопки фильтров по группам. Пользователи может отфильтровать записи по своим, групповым или всем
![ERPJS ListGroupFilter](img/ListGroupFilter.gif)

### Основные элементы взаимодействия с записью.
- Выпадающее меню выбора записи базовых полей ввода.
![ERPJS Input](img/Input.png)
Основные поля записи, которые подразумевают выбор идентификатора связанной записи таких как код товара, код клиента и т.д. имеют систему выпадающего списка для поиска и выбора нужного значения. При активации такого поля в его пределах отображается значок увеличительного стекла - кликая по которому откроется список записей того регистра, данные которого должны быть введены в это поле. При двойном клике на нужной записи в поле подставиться нужное значение. Вызов данного окна возможен по двойному нажатию клавиши ctrl (control).

- Выпадающее меню выбора записи поля в матрице.
![ERPJS Input](img/MatrixInput.png)

Многие поля в матрицах также связанны с другими регистрами и поддерживают выпадающее меню выбора и поиска. На данный момент доступен вызов этого меню только по двойному нажатию клавиши ctrl (control).

- Быстрый переход на связанную запись из поля в котором доступен вызов выпадающего окна выбора записи. Если в поле есть данные, тогда при активации поля курсором при нажатии клавиши F1 откроется связанная запись. Если для этой записи предусмотрено окно просмотра записи и у пользователя есть права на просмотр данной записи.

### Работа с матрицей документа
В некоторых документах есть такой элемент как матрица. Матрица представляет из себя бесконечную таблицу внутри записи с определенным набором столбцов. Например список товаров в документе продажи. 

- <B>Сортировка строк.</B> По умолчанию при открытии записи строки в матрице отсортированы в порядке их добавления в документ. Если нужно отсортировать данные для просмотра в другом порядке, нужно кликнуть на названии поля матрицы один или два раза. Данные будут отсортированы в порядке по этому полю.
![ERPJS Matrix Sort Row](img/MatrixSortRow.gif)

- <B>Удаление строки в матрице.</B>Чтобы удалить строку в матрице нужно нажать на пустую ячейку в начале сроки. при этом вся строка выделиться. После этого нужно нажать клавишу backspace(delete)
![ERPJS Matrix Delete Row](img/MatrixDeleteRow.gif)

- <B>Добавление строки в середину списка.</B> Чтобы добавить новую строку выберите строку, которую нужно сдвинуть вниз. После этого нажмите сочетание клавиш cntr+enter (control + return). Строки включая выбранную сдвинутся вниз.
![ERPJS Matrix Insert Row](img/MatrixInsertRow.gif)

- <B>Поиск данных в матрице.</B> В случае если есть необходимость найти строку содержащую некие данные предусмотрен поиск данных по матрицу. Нужно нажать кнопку с изображением увеличительного стекла в правом верхнем углу матрицы и в открывшееся поле ввести искомый текст.
![ERPJS Matrix Search Row](img/MatrixSearchRow.gif)
## Структура системы
____

ERPJS-4 имеет модульную структуру навигации. Все документы, настрйоки и отчеты логически расположены в соответствующих модулях.
###Модуль логически разбит на следующие элементы:
- Регистр - документы в системе. В одном регистре может содержаться множество однотипных записей.
- Настройка - документ в базе данных в единичном экземпляре отвечающий за различные астройки технических или бизнес процессов.
- Отчеты - процедуры для вывода отчетов.
- Обслуживание - процедуры для выполнения обслуживающих операций с данными.


### Модули:
- [Заказы на обслуживание](./modules/ServiceOrders.md)
- [Расчеты с клиентами](./modules/NominalLedger.md)
- [Расчеты с поставщиками](./modules/PurchaseLedger.md)
- [Система](./modules/System.md)
- [Склад](./modules/Stock.md)
- [Финансы](./modules/NominalLedger.md)
- [Ценообразование](./modules/Pricing.md)



