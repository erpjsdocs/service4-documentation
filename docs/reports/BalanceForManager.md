# Отчёт для менеджера
Отчет предназначен для отображения списка оплат при возвратахили дополнительных работах.

С помощью отчета проводится анализ завершенных заказов и начисленич бонусов для менеджеров.

При нажатии на номер Заказа на обслуживание открывается отдельнаый Заказ на обслуживание.

Определение отчета содержит:
- Период
- Счет
- клиента
- Сер. мастера
- Приемщика
- Товар
- Бренд товара
- Метод сортировки
- Виды заказа по оплатам
- Статус завершения заказа
- Статус заказа

Отчет содержит:

- Информацию по Заказу на обслуживание
- Тип и номер документа (кликая можно открыть транзакцию)
- Дату оплат которые закрыли Заказ на обслуживание 
- Разницу между первой оплатой закрывшей счет-фактуру и последующими оплатами. 
-Сумму разниц по всем заказам
